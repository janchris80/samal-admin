@extends('admin.app')
@section('content-header')
    <h1>Events<small>List</small></h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Events</li>
    </ol>
@stop
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

            <div class="box box-info">
                <div class="box-header">
                    <button class="btn btn-success" id="addmodal">
                        <i class="fa fa-calendar-check-o"></i> Add Event
                    </button>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Event Name</th>
                                <th>Location</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($events as $event)
                                <tr>
                                    <td>{{ $event->name }}</td>
                                    <td>{{ $event->location_name }}</td>
                                    <td>{{ date('h:i:s A - F d, Y', strtotime($event->date.' '.$event->time)) }}</td>
                                    <td>
                                        <button class="btn btn-sm btn-info" data-target="#edit-modal{{ $event->id }}" data-toggle="modal" data-show="tooltip" data-placement="top" title="Edit this event">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" data-target="#delete-modal{{ $event->id }}" data-toggle="modal" data-show="tooltip" data-placement="top" title="Delete this event">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                {{--delete modal--}}
                                <div class="modal" id="delete-modal{{ $event->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header text-center">
                                                <h5 class="modal-title text-center">Delete Confirmation</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-center">
                                                <p>Are you sure you want to delete?</p>
                                                <form class="d-inline-block" method="post" action="{{ route('events.destroy', $event->id) }}">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-sm btn-success">Delete</button>
                                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- edit modal -->
                                <div class="modal fade" id="edit-modal{{ $event->id }}">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Update Event</h4>
                                            </div>
                                            <form method="post" action="{{ route('events.update', $event->id) }}">
                                                @csrf
                                                @method('put')
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="name">Event Name</label>
                                                        <input type="text" class="form-control" name="name" id="name" value="{{ $event->name }}" autocomplete="off" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="location_id">Baranggay</label>
                                                        <select class="form-control select2" name="location_id" id="location_id" style="width: 100%;" autocomplete="off" required>
                                                            <option value="">Select Location</option>
                                                            @foreach ($locations as $location)
                                                                <option value="{{ $location->id }}" {{ $location->id == $event->location_id ? 'selected' : '' }}>{{ $location->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Date:</label>

                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" name="date" value="{{ $event->date }}" id="datepicker" autocomplete="off" required>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                    <div class="bootstrap-timepicker">
                                                        <div class="form-group">
                                                            <label for="time">Time picker:</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control timepicker" name="time" value="{{ $event->time }}" id="time" autocomplete="off" required>
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-clock-o"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $events->links() }}
                </div>
            </div>
        </div>
    </div>

    <!-- add modal -->
    <div class="modal fade" id="add-modal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Event</h4>
                </div>
                <form method="post" action="{{ route('events.store') }}">
                    @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Event Name</label>
                        <input type="text" class="form-control" name="name" id="name" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="location_id">Baranggay</label>
                        <select class="form-control select2" name="location_id" id="location_id" style="width: 100%;" autocomplete="off" required>
                            <option value="">Select Location</option>
                            @foreach ($locations as $location)
                                <option value="{{ $location->id }}">{{ $location->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Date:</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" name="date" id="datepicker" autocomplete="off" required>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="time">Time picker:</label>
                            <div class="input-group">
                                <input type="text" class="form-control timepicker" name="time" id="time" value="12:00" autocomplete="off" required>
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- edit modal -->

@stop

@section('script')
    <script>
        $(document).ready(function() {

            $(function () {
                $('[data-show="tooltip"]').tooltip()
            });

            let table = $('#addmodal').on('click', function () {
                $('#add-modal').modal('show');
            });

            //Datepicker
            $('#datepicker').datepicker({
                autoclose: true
            });
            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            });
            // Select2
            $('.select2').select2();
        } );

    </script>
@stop
